# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from datetime import datetime, timedelta
from django.views.decorators.csrf import csrf_exempt
from urlparse import urlparse, parse_qs, parse_qsl
import json

from django.http import HttpResponse, JsonResponse

from events.models import Event


def events(request, session=None):
    land_kwargs = {}


    if session:
        land_kwargs['session'] = session

    land_kwargs['pub_date__date'] = datetime.today()
    if "yesterday" in request.GET:
        land_kwargs['pub_date__date'] = datetime.today() - timedelta(days=1)

    if "date" in request.GET:
        land_kwargs['pub_date__date'] = datetime.datetime.strptime(request.GET["date"], "%d.%m.%Y").date()

    if "category" in request.GET:
        land_kwargs['category'] = request.GET["category"]

    if "event" in request.GET:
        land_kwargs['event'] = request.GET["event"]

    if "label" in request.GET:
        land_kwargs['event'] = request.GET["label"]

    if "session" in request.GET:
        land_kwargs['event'] = request.GET["session"]

    if "id" in request.GET:
        land_kwargs['landingpagepart__id'] = request.GET["id"]

    if "number" in request.GET:
        number = request.GET["number"]
    else:
        number = 100

    events = Event.objects.filter(**land_kwargs)[:number]

    return render_to_response('land/events/events.html', {
        'events': events,
    })



@csrf_exempt
def send_event(request):
    '''
    Send Event
    :param request:
    :return:
    '''
    result = False

    try:
        # json_data = json.loads(request.body)
        json_data = dict(parse_qsl(request.body))

        event = Event(
            category=json_data["category"].encode('utf-8') if 'category' in json_data else "",
            event=json_data["event"].encode('utf-8') if 'event' in json_data else "",
            label=json_data["label"].encode('utf-8') if 'label' in json_data else "",
            # http_referrer=json_data["http_referer"].encode('utf-8') if 'http_referer' in json_data else "",
            http_referrer=request.META["HTTP_REFERER"] if 'HTTP_REFERER' in request.META else "",
            path=request.get_full_path(),
            json = json.dumps(request.META, default=lambda o: None)
        )

        event.save()

        result = True
    except Exception, e:
        result = e

    return HttpResponse(result)
