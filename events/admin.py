__author__ = 'hramik'
from django.contrib import admin

from events.models import *


class EventAdmin(admin.ModelAdmin):
    list_display = ('pub_date', 'category', 'event', 'label', 'path', 'http_referrer')
    list_filter = ('category', 'event', 'pub_date', )
    search_fields = ('http_referrer',)


admin.site.register(Event, EventAdmin)
