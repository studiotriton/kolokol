# -*- coding: utf-8 -*-

import datetime, sys, os

from django.db import models
from django.contrib.auth.models import User

from django.contrib.postgres.fields import JSONField


class Event(models.Model):

    category = models.CharField(
        max_length=255,
        default="",
        blank=True,
        db_index=True,
    )

    event = models.CharField(
        max_length=255,
        default="",
        blank=True,
        db_index=True,
    )

    label = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    additional = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,

    )


    session = models.CharField(
        max_length=255,
        default="",
        blank=True
    )


    json = JSONField(
        # load_kwargs={'object_pairs_hook': collections.OrderedDict},
        blank=True,
        default={},
        null=True,
    )

    http_referrer = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    path = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    def __unicode__(self):
        return "%s" % (self.id,)

    class Meta:
        ordering = ["-pub_date"]
