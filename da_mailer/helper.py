# -*- coding: utf-8 -*-

# TODO:
# Поместить в заголовок письма вот такую строчку:
# List-Unsubscribe: <mailto:rm-0bxg2hykazs74dxau3ua1cq2cxdkebs@e.victoriassecret.com>
# Письмо на указанный адрес должно отписывать данного конкретного пользователя от подписки.

# send_notification_mail
# send_mail_to_admin
# send_mail_to_super_admin
# send_mail_to_author
# send_mail_to_user
# send_mail_to_group
# send_subscribe_mail

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from da_mailer.constants import *
from da_mailer.models import *
from django.contrib.auth.models import User



class DaMailer(models.Model):
    pass

    @classmethod
    def send_notification_mail(cls, name, data=False):
        '''
        Send email to admins and author of parent node
        '''
        cls.send_mail_to_admin(name, data)
        cls.send_mail_to_author(name, data)
        return True

    @classmethod
    def send_mail_to_admin(cls, template, subject=False, data=False):
        '''
        Send email to all admins
        '''
        notification_mail_type = None
        try:
            notification_mail_type = EmailNotificationType.objects.get(name=template)
            subject = notification_mail_type.admin_subject
            template_name = "admin_%s" % notification_mail_type.name
        except:
            template_name = template

        ADMINS = User.objects.filter(is_superuser=True).values_list('email', flat=True)

        mail_body = get_template("da_mailer/%s.mail.html" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            ADMINS,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_address(cls, template=False, subject=False, data=False, email=False):
        '''
        Send email to email address
        '''
        notification_mail_type = None
        template_name = template

        mail_body = get_template("%s" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            (email,),
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_super_admin(cls, template, subject=False, data=False):
        '''
        Send email to super admins
        '''
        notification_mail_type = None
        try:
            notification_mail_type = EmailNotificationType.objects.get(name=template)
            subject = notification_mail_type.admin_subject
            template_name = "admin_%s.mail.html" % notification_mail_type.name
        except:
            template_name = template

        ADMINS = User.objects.filter(is_superuser=True).values_list('email', flat=True)

        mail_body = get_template("%s" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            SUPER_ADMINS,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_author(cls, name, data=False):
        '''
        Send email to parent node author
        '''
        notification_mail_type = EmailNotificationType.objects.get(name=name)
        AUTHOR = User.objects.filter(id = data.user.id, is_superuser=False).values_list('email', flat=True)

        mail_body = get_template("da_mailer/author_%s.mail.html" % notification_mail_type.name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )
        subject = notification_mail_type.user_subject

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            TEST_RECEPIENT,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)


    @classmethod
    def send_mail_to_user(cls, template, subject, user, data=False):
        '''
        Send email to user
        '''

        mail_body = get_template("da_mailer/%s.mail.html" % template).render(
            Context({
                'data': data,
                'user': user,
            })
        )
        subject = subject

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            TEST_RECEPIENT,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to user')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True


    @classmethod
    def send_mail_to_group(cls, template, subject, group_name, data=False):
        '''
        Send email to user
        '''

        recepients = list(User.objects.filter(groups__name = group_name).values_list('email', flat=True))

        mail_body = get_template("da_mailer/%s.mail.html" % template).render(
            Context({
                'data': data,
            })
        )
        subject = subject

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            recepients,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to user')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True

    @classmethod
    def send_subscribe_mail(cls, name='subscribe_node', user=False, data=False):
        '''
        Send email to parent node author
        '''

        mail_body = get_template("da_mailer/subscribe_node.mail.html").render(
            Context({
                'data': data,
                'user': user
            })
        )
        subject = "Новый материал: %s «%s»" % (data.node_type, data.title)

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            SENDER,
            [user.email]
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send(fail_silently=True)
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True
