# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F
import operator

from django.utils.html import strip_tags

from django.core.cache import cache
from django.views.decorators.cache import cache_page

from django.db import transaction

from xml.dom import minidom
from bs4 import BeautifulSoup

from django.shortcuts import get_object_or_404

import json
from datetime import datetime
import random
from django.contrib.auth.decorators import login_required

from itertools import chain
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string

from django.contrib.sites.models import get_current_site
from django.core.files import File
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from ipgeo.models import Range
from django.db.models import Max
from django.db.models import F

from djangular.views.crud import NgCRUDView
from pprint import pprint
import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.http import HttpResponse


from da_mailer.helper import *


def test(request):

    result = "False"
    DaMailer.send_mail_to_admin(template = 'new_freelancer_notification', subject = u'Новый фрилансер')

    result = "True"

    return HttpResponse(result)

