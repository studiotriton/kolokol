# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin

from django.db import models

from mptt.admin import MPTTModelAdmin

from menu.models import *


class MenuGroupAdmin(admin.ModelAdmin):
    pass


class MenuAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'link', 'image', 'position', 'menu_group', 'publish')
    list_filter = ('menu_group', 'level', )
    list_editable = ('publish', 'slug', 'position')

admin.site.register(MenuGroup, MenuGroupAdmin)
admin.site.register(Menu, MenuAdmin)