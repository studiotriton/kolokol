# -*- coding: utf-8 -*-
__author__ = 'hramik'

from menu.models import *

def menu_processor(request):
    try:
        main_menu = Menu.objects.filter(menu_group__slug='main', publish=1)
    except Exception, e:
        main_menu = False

    try:
        current_menu = Menu.objects.select_related().get(link=request.path)
    except Exception, e:
        current_menu = False

    return {
        'main_menu': main_menu,
        'current_menu': current_menu,
    }
