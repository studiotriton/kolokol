/**
 * Created by hramik on 27/07/16.
 */
/**
 * Created by hramik on 23/09/15.
 */


/**
 * Scroll page to top
 * @returns {boolean}
 */
var toTop = function () {
    // Add offset calculation if there is fixed top layer on page
    // var offset = $(".pagepart.fixed").outerHeight();
    var offset = 0;
    $("html,body").animate({scrollTop: 0 - offset}, "400");
    event.stopImmediatePropagation();
    event.preventDefault();
    return false;
};

$(document).ready(function () {

});