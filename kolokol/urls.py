"""kolokol URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from kolokol import settings
from django.conf.urls import  include, url, handler404
from kolokol.views import *
from django.views import static

urlpatterns = [

    url(r'^$', page, name='home'),

    url(r'^clear_cache$', clear_cache, name='clear_cache_page'),

    # url(r'^sitemap$', sitemap, name='sitemap'),

    url(r'^robots\.txt$', robots, name='robots'),

    url(r'^sitemap\.xml$', sitemap_xml, name='sitemaps'),

    url(r'^save_page/(?P<id>[0-9]+)$', save_page, name='save_page'),

    url(r'^events/', include('events.urls', namespace='events')),

    url(r'^admin/', include(admin.site.urls)),



    url(r'^static/(?P<path>.*)$', static.serve, {
        'document_root': settings.STATIC_ROOT,
    }),

    url(r'^media/(?P<path>.*)$', static.serve, {
        'document_root': settings.MEDIA_ROOT,
    }),

    url(r'^search-(?P<searh_string>.+)$', search, name="search"),  # products or static page
    url(r'^(?P<slug>[a-zA-Z0-9_\-\/]+)_page(?P<pagination_page>[0-9]+)$', page, name="page_page"),  # products or static page
    url(r'^(?P<slug>[a-zA-Z0-9_\-\/]+)$', page, name="page"),  # products or static page



    # url(r'^facebook/', include('django_facebook.urls')),
    # url(r'^accounts/', include('django_facebook.auth_urls')),

]

handler404 = error404