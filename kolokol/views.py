# -*- coding: utf-8 -*-
__author__ = 'hramik'

from django.db import models
from django.db.models import Q, F

from django.http import Http404, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect, render
from django.core.mail import send_mail
from django.template import Context, Engine, Template as djangoTemplate
from django.template.loader import get_template
import random
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required

from django.conf import settings
from django.core.cache import cache
from django.views.decorators.cache import cache_page

from node.models import *
from config.models import SiteConfiguration
import json
import requests

import json


def last_news():
    return Page.objects.all()[:3]


# def home(request):
#
# return redirect('page', slug='about')

# return render(request, 'node/pages/home.html', {
#     'news_list': Page.objects.filter(tags__slug__in=('news',), publish=True)[:3],
#     'slideshow': Page.objects.filter(tags__slug__in=('slide',), publish=True).order_by('position')[:3],
#     'videos': Page.objects.filter(tags__slug__in=('video',), publish=True).order_by('position')[:3],
#     'on_main': Page.objects.filter(tags__slug__in=('on_main',), publish=True).order_by('pub_date')[:6],
#     'tags': Tag.objects.filter(show=True),
# },
#
#                       context_instance=RequestContext(request))
@login_required
def clear_cache(request):
    cache.clear()
    return HttpResponse('True')


def page(request, slug=None, pagination_page=1):
    '''
    Page View
    :param request:
    :param slug:
    :return:
    '''

    cache_key = 'land:%s-%s' % (slug, pagination_page)
    page = cache.get(cache_key)

    if page and not request.user.is_staff:
        pass
        # return page

    # Form
    if request.POST and not request.POST["form[name]"] == 'seo':

        # Create page
        if "node[create]" in request.POST and request.POST['node[create]'] == 'true':
            node_kwargs = {}

            try:
                node_tag = Tag.objects.get(slug=request.POST['node[tag]'])
                node_kwargs['main_tag'] = node_tag
            except Exception, e:
                print Exception, e
                node_tag = None

            try:
                node_kwargs['title'] = request.POST['title']
            except Exception, e:
                node_kwargs['title'] = "%s %s %s" % (node_tag.name, datetime.now(), str(random.randrange(0, 1001, 3)))

            try:
                node_kwargs['publish'] = request.POST['node[publish]']
            except Exception, e:
                print Exception, e

            if 'teaser' in request.POST:
                node_kwargs['teaser'] = request.POST['teaser']

            if 'body' in request.POST:
                node_kwargs['body'] = request.POST['body']

            if 'g-recaptcha-response' in request.POST:
                secret = SiteConfiguration.objects.get().recapcha_secret
                response = request.POST['g-recaptcha-response']
                remoteip = request.META.get('REMOTE_ADDR')

                url = 'https://www.google.com/recaptcha/api/siteverify'
                data = {'secret': secret, 'response': response, 'remoteip': remoteip}
                # headers = {'Content-Type': 'application/json'}

                r = requests.post(url, data=data)
                content = json.loads(r.text)

                try:
                    if content['success']:
                        new_page = Page(**node_kwargs)
                        new_page.save()
                        
                        new_page.tags.add(node_tag)
                        print 'new_page saved'
                    else:
                        print 'new_page NOT saved', content
                except Exception, e:
                    print Exception, e


        # Send mail
        try:
            if "form[send_mail]" in request.POST and request.POST['form[send_mail]'] == 'true':

                mail_body = None

                if 'email[template]' in request.POST:
                    try:
                        template = Template.objects.get(slug=request.POST['email[template]'])

                        mail_body = djangoTemplate(template.template).render(
                            Context({
                                'data': request.POST,
                            })
                        )
                    except Exception, e:
                        print Exception, e

                if not mail_body:
                    mail_body = get_template("node/email/form.html").render(
                        Context({
                            'data': request.POST,
                        })
                    )

                if 'email[emails]' in request.POST:
                    to = request.POST['form[emails]'].split()
                else:
                    to = ['hramik@gmail.com']

                if 'email[subject]' in request.POST:
                    subject = request.POST['form[subject]']
                else:
                    subject = "Заполнена новая форма"

                send_mail(subject, mail_body, settings.DEFAULT_FROM_EMAIL, to)
        except Exception, e:
            pass

    front = False
    seo_target = None


    if slug == SiteConfiguration.objects.get().main_page:
        # raise Http404
        slug = SiteConfiguration.objects.get().non_found_page

    if slug == '404':
        # raise Http404
        slug = SiteConfiguration.objects.get().non_found_page

    if not slug:
        slug = SiteConfiguration.objects.get().main_page
        front = True

    try:
        tag = Tag.objects.get(slug=slug)
        seo_target = tag
        if not tag.create_page:
            tag = None
            seo_target = None
    except:
        tag = None

    try:
        page = Page.objects.select_related().get(slug=slug)
        if page.body_clear:
            page.body = "{% load hramik_tags %}" + page.body
        seo_target = page
    except:
        page = None
        if not page and not tag:
            page = Page.objects.select_related().get(slug=SiteConfiguration.objects.get().non_found_page)
            if page.body_clear:
                page.body = "{% load hramik_tags %}" + page.body
            seo_target = page
            slug = '404'

    # Save SEO info
    if request.POST and request.POST["form[name]"] == 'seo' and seo_target:

        if 'seo[h1]' in request.POST:
            seo_target.title = request.POST['seo[h1]']
            seo_target.name = request.POST['seo[h1]']

        if 'seo[h2]' in request.POST:
            seo_target.second_title = request.POST['seo[h2]']

        if 'seo[page_title]' in request.POST:
            seo_target.page_title = request.POST['seo[page_title]']

        if 'seo[meta_desc]' in request.POST:
            seo_target.meta_desc = request.POST['seo[meta_desc]']

        if 'seo[meta_keywords]' in request.POST:
            seo_target.meta_keywords = request.POST['seo[meta_keywords]']

        if 'seo[noindex]' in request.POST:
            seo_target.noindex = True
        else:
            seo_target.noindex = False

        if 'seo[sidebar]' in request.POST:
            seo_target.sidebar = True
            if not seo_target.sidebar_template:
                template = Template.objects.get(slug="banners")
                seo_target.sidebar_template = template
                seo_target.save()
                
        else:
            seo_target.sidebar  = False

        if 'seo[slug]' in request.POST:
            if not seo_target.slug == request.POST['seo[slug]']:
                seo_target.slug = request.POST['seo[slug]']
                seo_target.save()
                
                if page:
                    return redirect('page', slug=seo_target.slug)
                if tag:
                    return redirect('page', slug=seo_target.slug)

        seo_target.save()
        

    items_type = 'cards'

    if request.GET and 'type' in request.GET:
        items_type = request.GET['type']

        # NODE TYPE LIST
        # try:
        #     node_type = NodeType.objects.get(slug=slug)
        #     items = Page.objects.filter(node_type=node_type)
        #
        #     if not 'type' in request.GET:
        #         items_type = node_type.view_type
        #
        #     return render(request, 'kolokol/pages/items.html', {
        #         'entity': node_type,
        #         'items': items,
        #         'items_type': items_type,
        #     },
        #                               context_instance=RequestContext(request))
        # except:
        #     pass

        # TAG LIST

    if tag:
        order = tag.order.split(",")
        item_list = Page.objects.filter(tags=tag).order_by(*order)

        if tag.pagination:
            paginator = Paginator(item_list, tag.pagination_page_num)
            items = paginator.page(pagination_page)
        else:
            items = item_list

        if not 'type' in request.GET:
            items_type = tag.view_type

        response = render(request, 'node/pages/items.html', {
            'page': tag,
            'items': items,
            'items_type': items_type,
            'columns': 'three',
            'request': request
        })

        cache.set(cache_key, response)
        return response

    if page:

        response = render(request, 'node/pages/page.html', {
            'page': page,
            'front': front,
            'request': request
        })

    if slug == SiteConfiguration.objects.get().non_found_page:
        response.status_code = 404

    cache.set(cache_key, response)
    return response

    # raise Http404


def error404(request):
    redirect('page', slug='404')

def save_page(request, id=None):
    if request.POST:
        page = Page.objects.get(id=id)

        if "save_header" in request.POST:
            page.title = strip_tags(request.POST["save_header"]).strip()

        if "save_content" in request.POST:
            page.body = re.sub('id="editable_body">(.+)<\/div>', 'id="editable_body">%s</div>' % request.POST["save_content"], page.body, flags=re.U + re.S)

        page.save()
        

    return HttpResponse('ok')


def search(request, search_string):
    # NODE SEARCH LIST
    items = Page.objects.filter(body__contains=search_string)

    return render(request, 'node/pages/items.html', {
        'entity': u"Поиск: %s" % search_string,
        'items': items,
    })


def robots(request):
    '''
    Robots.txt
    :param request:
    :return:
    '''
    robots = SiteConfiguration.objects.get().robots

    return HttpResponse(robots, content_type="text/plain")

def sitemap_xml(request):
    '''
    sitemaps.xml
    :param request:
    :return:
    '''

    pages = Page.objects.all()

    return render(request, 'node/pages/sitemap_xml.html', {
        'request': request,
        'pages': pages,
        'scheme': request._get_scheme()
    }, content_type='text/xml')


def sitemap(request):
    return render(request, 'node/pages/sitemap.html', {
        'request': request,
        'page': None
    })
