# -*- coding: utf-8 -*-

__author__ = 'hramik'
from django import template
# from django.template.defaultfilters import simple_tag
import logging
import os
import os.path
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

import re
from sorl.thumbnail import get_thumbnail
from django import template
from datetime import datetime, timedelta

from django.template.defaultfilters import stringfilter
from django.template import Context, Template, loader
from node.models import *
from config.models import *

register = template.Library()
location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)


@register.simple_tag(takes_context=True)
def render(context, value):
    return Template(value).render(context)


# @register.inclusion_tag('node/items/items.html', takes_context=True)
@register.simple_tag(takes_context=True)
def show_items(context, *args, **kwargs):
    items_kwargs = {}
    exclude_kwargs = {}

    if "exclude" in kwargs:
        exclude_kwargs['id__in'] = str(kwargs['exclude']).split()

    if "exclude_tags" in kwargs:
        exclude_kwargs['tags__slug__in'] = str(kwargs['exclude_tags']).split()

    if "tags" in kwargs:
        items_kwargs['tags__slug__in'] = kwargs['tags'].split(',')

    if "tags_ids" in kwargs:
        items_kwargs['tags__id__in'] = str(kwargs['tags_ids']).split(',')

    if "start" in kwargs:
        start = kwargs["start"]
    else:
        start = 0

    if "publish" in kwargs:
        items_kwargs['publish'] = kwargs["publish"]
    else:
        pass

    if "end" in kwargs:
        end = kwargs["end"]
    else:
        end = 0

    if "col" in kwargs:
        end = int(start) + int(kwargs["col"])

    if "template" in kwargs:
        template = Template.objects.get(slug=kwargs['template'])
    else:
        template = None

    if not template:
        if "type" in kwargs:
            type = 'node/items/%s.html' % kwargs['type']
        else:
            type = 'node/items/items.html'
    else:
        type = None

    if "order" in kwargs:
        order = kwargs['order']
    else:
        order = 'position'

    if "model" in kwargs:
        model = kwargs['model']
    else:
        model = 'Page'

    if "not_preview" in kwargs:
        not_preview = kwargs['not_preview']
    else:
        not_preview = False

    if "not_desc" in kwargs:
        not_desc = kwargs['not_desc']
    else:
        not_desc = False

    if "not_time" in kwargs:
        not_time = kwargs['not_time']
    else:
        not_time = False

    if "not_title" in kwargs:
        not_title = kwargs['not_title']
    else:
        not_title = False

    if "target" in kwargs:
        target = kwargs['target']
    else:
        target = False

    if "classes" in kwargs:
        classes = kwargs['classes']
    else:
        classes = False

    if "with_avatar" in kwargs:
        with_avatar = kwargs['with_avatar']
    else:
        with_avatar = None

    if "columns" in kwargs:
        columns = kwargs['columns']
    else:
        columns = "three"

    if model == 'Tag':
        items_kwargs["show"] = True
        if end and start:
            items = Tag.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[start:end]
        elif end:
            items = Tag.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[:end]
        elif start:
            items = Tag.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[start:]
        else:
            items = Tag.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)
    else:
        if end and start:
            items = Page.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[start:end]
        elif end:
            items = Page.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[:end]
        elif start:
            items = Page.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)[start:]
        else:
            items = Page.objects.filter(**items_kwargs).exclude(**exclude_kwargs).order_by(order)

    if type:
        t = context.template.engine.get_template(type)
    else:
        t = context.template.engine.from_string(template.template)

    return t.render(Context({
        'items': items,
        'not_preview': not_preview,
        'not_desc': not_desc,
        'not_title': not_title,
        'not_time': not_time,
        'target': target,
        'columns': columns,
        'classes': classes,
        'with_avatar': with_avatar,
        'request': context['request'],
    }, autoescape=context.autoescape))


@register.simple_tag(takes_context=True)
def conf_file_url(context, *args, **kwargs):
    return ConfigurationFile.objects.get(name=args[0]).file.url

@register.simple_tag(takes_context=True)
def conf_file_url_today(context, *args, **kwargs):

    object = ConfigurationFile.objects.get(name=args[0])

    try:
        date = datetime.today().strftime("%d.%m.%Y")
        file = object.file
        base = os.path.basename(file.name)
        file_name, file_extension = os.path.splitext(base)
        new_base = "%s%s" % (args[1].replace("%date", date), file_extension)

        if not base == new_base:
            os.rename(object.file.path, object.file.path.replace(base, new_base))
            object.file.name = object.file.name.replace(base, new_base)
            object.save()
    except:
        pass

    return object.file.url

@register.simple_tag(takes_context=True)
def sitemap(context, *args, **kwargs):
    tags = Tag.objects.filter(show_in_sitemap=True)
    no_tag_pages = Page.objects.filter(main_tag=None)
    t = loader.get_template('node/pages/sitemap.html')
    return t.render(Context({
        'tags': tags,
        'no_tag_pages': no_tag_pages,
    }, autoescape=context.autoescape))


@register.filter(name='split')
def spit(string):
    return string.split(',')


@register.filter(name='strip')
def strip(value):
    text = re.sub("[^a-z0-9\.\,]+", "", value, flags=re.IGNORECASE)
    return text
