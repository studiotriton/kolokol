# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportActionModelAdmin


from django.db import models

from mptt.admin import MPTTModelAdmin
from sorl.thumbnail.admin import AdminImageMixin

from node.models import *


class AdditionalFilesInline(admin.TabularInline):
    model = AdditionalFile
    extra = 0
    sortable_field_name = "position"


class AdditionalPhotoInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalPhoto
    extra = 0
    sortable_field_name = "position"


class CommentAdditionalPhotoInline(AdminImageMixin, admin.TabularInline):
    model = CommentAdditionalPhoto
    extra = 0
    sortable_field_name = "position"


class AdditionalVideosInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalVideos
    extra = 0
    sortable_field_name = "position"


class PageAdmin(admin.ModelAdmin):
    '''
    Admin pages
    '''
    fieldsets = (

        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('publish', 'sidebar', 'template', 'sidebar_template'), ('title', 'show_title_in_list', 'show_title_in_page', 'second_title'), ('image', 'show_image_in_list', 'show_image_in_page'), 'video', 'teaser', ('body', 'body_clear'),
                       'additional_settings',
                       'slug', 'link',
                       ('comments_available', 'comments_engine',), ('pub_date', 'some_date'))
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('noindex', 'page_title', 'meta_desc', 'meta_keywords', ('priority', 'changefreq'))
        }),
        ('Tags', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('tags', 'main_tag', 'second_tag')
        }),
        ('User', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('user', 'edit_user'))
        }),
        # ('Additional', {
        #     'fields': ('json',)
        # }),
    )
    list_display = ('id', 'title', 'h1', 'page_title', 'tags_list', 'main_tag', 'second_tag',  'edit_date', 'pub_date', 'position', 'comments_available', 'page_url', 'sidebar', 'publish')
    list_display_links = ('id', 'title')
    list_filter = ('tags',)
    list_editable = ('publish', 'position', 'sidebar')

    filter_horizontal = ('tags',)
    search_fields = ('id', 'title', 'slug', 'main_tag')
    inlines = (AdditionalPhotoInline, AdditionalFilesInline)
    ordering = ('-edit_date',)

    def page_url(self, obj):
        return format_html("<a href='/{url}'>{url}</a>", url=obj.slug)

    page_url.short_description = "Page URL"
    page_url.allow_tags = True


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'prular', 'classes', 'view_type', 'template', 'page_template', 'slug', 'show', 'create_page', 'show_in_sitemap', 'sidebar', 'position')
    list_editable = ('name', 'prular', 'classes', 'view_type', 'template', 'page_template', 'slug', 'show', 'create_page', 'sidebar', 'show_in_sitemap', 'position')
    fieldsets = (

        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('name', 'prular', 'slug'), 'image', ('show', 'create_page', 'show_in_sitemap'), ('pagination', 'pagination_page_num'), 'classes', ('view_type', 'template', 'page_template', 'order'), 'desc', 'position')
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('noindex', 'page_title', 'meta_desc', 'meta_keywords', ('priority', 'changefreq'))
        })

        # ('Additional', {
        #     'fields': ('json',)
        # }),
    )


class TemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'path')

#  Resources

class TemplateResource(resources.ModelResource):
    class Meta:
        model = Template

admin.site.register(Page, PageAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Template, TemplateAdmin)
