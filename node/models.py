# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.core.cache import cache

from django.db import models
from django.forms import ModelForm
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django import forms
from autoslug import AutoSlugField
from transliterate import translit, get_available_language_codes
from django.utils.text import slugify

from sorl.thumbnail import ImageField
from PIL import Image
from autoslug import AutoSlugField
from datetime import date, datetime
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.html import format_html

# from django.contrib.sites.models import Site
# from django.contrib.sites.managers import CurrentSiteManager
from django.template.defaultfilters import truncatewords, truncatechars
from django.contrib.postgres.fields import JSONField

# from autoslug import AutoSlugField


from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

VIEW_TYPE_CHOICES = (
    ('cards', 'Карточки'),
    ('items', 'Список'),
    ('slideshow', 'Слайдшоу'),
    ('links', 'Ссылки'),
)


class ShowedTagsManager(models.Manager):
    def get_query_set(self):
        return super(ShowedTagsManager, self).get_query_set().filter(show=True)


class SEOFields(models.Model):
    '''
    SEO FIELDS
    '''
    noindex = models.BooleanField(
        default=False,
    )

    h1 = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'H1'
    )

    h2 = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'H2'
    )

    page_title = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        help_text=_("for windows title"),
        default='',
        verbose_name=u'Meta Title'
    )

    meta_desc = models.TextField(
        blank=True,
        null=True,
        help_text=_("for meta description"),
        verbose_name=u"Meta Description",
        default='',
    )

    meta_keywords = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        default=''
    )

    PRIORITY_VALUES = (
        ('0.1', '0.1'),
        ('0.2', '0.2'),
        ('0.3', '0.3'),
        ('0.4', '0.4'),
        ('0.5', '0.5'),
        ('0.6', '0.6'),
        ('0.7', '0.7'),
        ('0.8', '0.8'),
        ('0.9', '0.9'),
        ('1.0', '1.0'),
    )

    CHANGEFREQ_VALUES = (
        ('always', 'always'),
        ('hourly', 'hourly'),
        ('daily', 'daily'),
        ('weekly', 'weekly'),
        ('monthly', 'monthly'),
        ('yearly', 'yearly'),
        ('never', 'never'),
    )

    priority = models.CharField(
        max_length=3,
        default='0.5',
        blank=True,
        choices=PRIORITY_VALUES,
    )

    changefreq = models.CharField(
        max_length=10,
        default='weekly',
        blank=True,
        choices=CHANGEFREQ_VALUES,
    )

    class Meta():
        abstract = True


class Template(models.Model):
    name = models.CharField(
        max_length=255,
        default=''
    )

    slug = models.SlugField(
        unique=True
    )

    template = models.TextField(
        blank=True,
    )

    path = models.BooleanField(
        default=False
    )

    def __unicode__(self):
        return self.name


class Tag(SEOFields):
    '''
    Теги
    '''
    name = models.CharField(
        max_length=255,
        default=''
    )

    image = models.ImageField(
        upload_to='main_photos/',
        blank=True,
        null=True
    )

    prular = models.CharField(
        max_length=255,
        default='',
        blank=True
    )

    classes = models.CharField(
        max_length=255,
        default='',
        blank=True,
    )

    view_type = models.CharField(
        max_length=255,
        choices=VIEW_TYPE_CHOICES,
        default='cards',
        blank=True,
    )

    slug = models.SlugField(
        unique=True
    )

    desc = models.TextField(
        blank=True,
    )

    show = models.BooleanField(
        verbose_name="Show in list",
        default=False
    )

    create_page = models.BooleanField(
        verbose_name="Create Page with this tag",
        default=True
    )

    pagination = models.BooleanField(
        verbose_name="Create Pagination",
        default=True
    )

    pagination_page_num = models.PositiveIntegerField(
        verbose_name="Items on Page",
        default=10,
        blank=True,
    )

    show_in_sitemap = models.BooleanField(
        verbose_name="Show in sitemap",
        default=False
    )

    sidebar = models.BooleanField(
        default=False
    )

    template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='tag_template',
        verbose_name='Tempate for List Tags'
    )

    page_template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='page_template',
        verbose_name='Tempate for Page with Tag'
    )

    order = models.CharField(
        max_length=100,
        default="position,id",
    )

    position = models.PositiveSmallIntegerField(
        default=1,
    )

    objects = models.Manager()
    showed_objects = ShowedTagsManager()

    def __unicode__(self):
        return self.name

    def pages(self):
        return Page.objects.filter(main_tag=self)


'''
Выдача нод с комментариями
'''


class WithCommentsManager(models.Manager):
    def get_query_set(self):
        node_ids = Comment.objects \
            .filter(publish=True) \
            .values_list("node__id", flat=True)

        return super(WithCommentsManager, self).get_query_set().filter(id__in=node_ids)


class InitialFields(models.Model):
    '''
    Start field: title, body, publich, etc
    '''

    title = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'Заголовок'
    )

    body = models.TextField(
        verbose_name=u"Содержимое",
        blank=True,
        null=True,
        default=''
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )

    publish = models.BooleanField(
        verbose_name="Publish",
        default=True
    )

    class Meta():
        abstract = True

    def __unicode__(self):
        return self.title


class Node(models.Model):
    '''
    Нода — основополагающая сущность
    '''

    image = models.ImageField(
        upload_to='main_photos/',
        blank=True,
    )

    small_image = models.ImageField(
        upload_to='main_small_photos/',
        blank=True,
    )

    show_image_in_list = models.BooleanField(
        default=True,
    )

    show_image_in_page = models.BooleanField(
        default=True,
    )

    title = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'Заголовок'
    )

    show_title_in_list = models.BooleanField(
        default=True,
    )

    show_title_in_page = models.BooleanField(
        default=True,
    )

    second_title = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'Подзаголовок'
    )

    video = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'Видео'
    )

    photo = models.ImageField(
        upload_to='main_photos',
        blank=True
    )

    show_photo = models.BooleanField(
        default=True,
    )

    teaser = models.TextField(
        blank=True,
        null=True,
        default=''
    )

    body = models.TextField(
        verbose_name=_("Body"),
        blank=True,
        null=True,
        default=''
    )

    body_clear = models.BooleanField(
        default=False
    )

    tags = models.ManyToManyField(
        Tag,
        blank=True,
        verbose_name=u'Теги'
    )

    main_tag = models.ForeignKey(
        Tag,
        blank=True,
        null=True,
        verbose_name=u'Основной тег',
        default=None,
        related_name='main_tag'
    )

    second_tag = models.ForeignKey(
        Tag,
        blank=True,
        null=True,
        verbose_name=u'Вторичный тег',
        default=None,
        related_name='second_tag'
    )

    slug = models.SlugField(
        unique=False,
        blank=True,
        max_length=255,
        default=''
    )

    link = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'Ссылка'
    )

    position = models.PositiveSmallIntegerField(
        default=1,
    )

    additional_settings = JSONField(
        blank=True,
        default="",
        null=True,
    )

    # json = JSONField(
    #     blank=True
    # )


    # slug_with_dot = RegexValidator(r'^[0-9a-zA-Z_\.\-\/]*$',
    #                                'Only only letters, numbers, underscores, hyphens or dot are allowed.')
    #
    # dot_slug = models.CharField(
    #     blank=True,
    #     max_length=255,
    #     validators=[slug_with_dot],
    #     default='',
    # )


    user = models.ForeignKey(
        User,
        blank=True,
        default=1,
        null=True
    )

    some_date = models.DateTimeField(
        'some date',
        editable=True,
        blank=True,
        null=True,
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )
    pub_date.editable=True,


    edit_date = models.DateTimeField(
        'date editing',
        auto_now=True,
    )

    edit_user = models.CharField(
        max_length=255,
        blank=True,
        default='admin',
        null=True,
    )

    add_date = models.DateTimeField(
        'add date',
        auto_now_add=True,
    )

    comments_available = models.BooleanField(
        verbose_name="Comments Available",
        default=False
    )

    comments_engine = models.CharField(
        max_length=20,
        blank=True,
        choices=(
            ('disqus', 'Disqus'),
            ('self', 'Встроенные')
        ),
        default='disqus'

    )

    publish = models.BooleanField(
        verbose_name="Publish",
        default=True
    )

    def __unicode__(self):
        return self.title \
            # + '/' + str(self.id) + '/' + str(self.nid)

    def save(self, *args, **kwargs):
        cache.clear()
        if not self.slug:
            self.slug = slugify(translit(self.title, 'ru', reversed=True))
            # self.dot_slug = slugify_ru(self.title, to_lower=True)
        super(Node, self).save(*args, **kwargs)

    def tag_slugs(self):
        tags = []
        for tag in self.tags.all():
            tags.append(tag.slug)
        return tags

    def tags_list(self):
        return ', '.join([a.name for a in self.tags.all()])

    # def __init__(self, *args, **kwargs):
    # print kwargs.pop('user', None)
    # super(Node, self).__init__(*args, **kwargs)

    def get_publish_comments(self):
        return self.comment_set.filter(publish=True)

    def get_publish_comments_part(self):
        return self.comment_set.filter(publish=True)[0:50]

    def get_all_comments(self):
        return self.comment_set.all()

    def get_first_tag(self):
        return self.tags

    class Meta:
        permissions = (
            ("can_edit_wysiwyg", "Can edit through wysiwyg"),
        )
        ordering = ['title']


'''
Страница
'''


class Page(Node):
    noindex = models.BooleanField(
        default=False,
    )

    h1 = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'H1'
    )

    h2 = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'H2'
    )

    page_title = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        help_text=_("for windows title"),
        default='',
        verbose_name=u'Meta Title'
    )

    meta_desc = models.TextField(
        blank=True,
        null=True,
        help_text=_("for meta description"),
        verbose_name=u"Meta Description",
        default='',
    )

    meta_keywords = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        default=''
    )

    sidebar = models.BooleanField(
        default=False
    )

    template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='parent_page'
    )

    sidebar_template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='sidebar_page'
    )

    PRIORITY_VALUES = (
        ('0.1', '0.1'),
        ('0.2', '0.2'),
        ('0.3', '0.3'),
        ('0.4', '0.4'),
        ('0.5', '0.5'),
        ('0.6', '0.6'),
        ('0.7', '0.7'),
        ('0.8', '0.8'),
        ('0.9', '0.9'),
        ('1.0', '1.0'),
    )

    CHANGEFREQ_VALUES = (
        ('always', 'always'),
        ('hourly', 'hourly'),
        ('daily', 'daily'),
        ('weekly', 'weekly'),
        ('monthly', 'monthly'),
        ('yearly', 'yearly'),
        ('never', 'never'),
    )

    priority = models.CharField(
        max_length=3,
        default='0.5',
        blank=True,
        choices=PRIORITY_VALUES,
    )

    changefreq = models.CharField(
        max_length=10,
        default='weekly',
        blank=True,
        choices=CHANGEFREQ_VALUES,
    )

    def show_sidebar(self):
        for tag in self.tags.all():
            if tag.sidebar:
                return True
        return False

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return reverse('nodes:node', args=[self.dot_slug])

    class Meta:
        ordering = ["-pub_date"]
        permissions = (("can_edit_seo", "Can edit SEO"),
                       ("can_edit_slug", "Can edit slug"),
                       ("can_edit_sidebar", "Can edit sidebar"),)


'''
Фото/картинка
'''


class Photo(models.Model):
    name = models.CharField(
        max_length=200,
        blank=True,
        default=''
    )

    photo = models.ImageField(
        upload_to='photos/'
    )

    desc = models.TextField(
        blank=True,
        default=''
    )

    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


'''
Дополнительные файлы
'''


class AdditionalFile(models.Model):
    node = models.ForeignKey(
        Node,
        related_name='files')


    name = models.CharField(
        max_length=255,
        blank=True,
    )

    file = models.FileField(
        upload_to=_("additional_files"),
        blank=True,
    )

    link = models.CharField(
        max_length=1024,
        blank=True,
    )


    title = models.CharField(max_length=250, blank=True, default='')


    desc = models.TextField(
        blank=True,
        default=''
    )

    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


'''
Дополнительные фото
'''


class AdditionalPhoto(models.Model):
    node = models.ForeignKey(
        Node,
        related_name='photos'
    )

    photo = models.ImageField(
        upload_to='nodephotos',
        blank=True,
    )

    link = models.CharField(
        max_length=1024,
        blank=True,
    )

    alt = models.CharField(max_length=250, blank=True, default='')

    title = models.CharField(max_length=250, blank=True, default='')

    position = models.PositiveSmallIntegerField("Position", default=0)

    desc = models.TextField(
        blank=True,
        default=''
    )

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return self.photo.url


'''
Дополнительное видео
'''


class AdditionalVideos(models.Model):
    VIDEO_TYPES = (
        ('youtube', 'youtube'),
        ('mp4', 'mp4'),
        ('webm', 'webm'),
    )
    node = models.ForeignKey(Node)
    video_url = models.CharField(max_length=500, verbose_name=_("Video URL"), default='')
    video_poster = models.ImageField(upload_to='nodevideo_posters', blank=True, default="images/misc/empty.png")
    video_type = models.CharField(max_length=20, default='youtube', choices=VIDEO_TYPES)
    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


RATE_CHOICES = (
    ('1', _('никуда не годится')),
    ('2', _('плохо')),
    ('3', _('удовлетворительно')),
    ('4', _('хорошо')),
    ('5', _('отлично')),
)

'''
Комментарий
'''


class Comment(models.Model):
    node = models.ForeignKey(Node)
    title = models.CharField(max_length=200, blank=True, default="")
    body = models.TextField(verbose_name=_("Body"), blank=True, default="")
    user = models.ForeignKey(User, blank=True, default=0, null=True)
    author = models.CharField(max_length=200, blank=True, default="")
    email = models.EmailField(max_length=400, blank=True, default="")

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )

    publish = models.BooleanField(verbose_name="Publish", default=True)

    def __unicode__(self):
        return self.body

    class Meta:
        ordering = ['pub_date']


class CommentForm(ModelForm):
    '''
    Форма комментария
    '''
    node = forms.IntegerField(widget=forms.HiddenInput(attrs={'class': '', 'ng-model': 'comment.node'}), label="")
    body = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'validate[required]', 'ng-model': 'comment.body', 'required': 'True', 'msd-elastic': 'true'}),
        label="Комментарий")

    class Meta:
        model = Comment
        fields = ('body',)


'''
Форма анонимного комментария
'''


class AnonimousCommentForm(ModelForm):
    node = forms.IntegerField(widget=forms.HiddenInput(attrs={'class': '', 'ng-model': 'comment.node'}), label="")
    author = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate[required]', 'ng-model': 'comment.author', 'required': 'True'}),
        label='Имя')
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'comment.email', 'required': 'True', 'type': 'email'}))
    body = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'validate[required]', 'ng-model': 'comment.body', 'required': 'True', 'msd-elastic': 'true'}),
        label="Комментарий")

    class Meta:
        model = Comment
        fields = ('author', 'email', 'body')


'''
Лайк для комментария
'''


class CommentLike(models.Model):
    comment = models.ForeignKey(Comment)
    user = models.ForeignKey(User, default=0)
    like_date = models.DateTimeField('date published', auto_now_add=True)

    def __unicode__(self):
        return self.user.username + "/" + str(self.comment.id)

    class Meta:
        ordering = ['like_date']


'''
Дополнительные фото для комментария
'''


class CommentAdditionalPhoto(models.Model):
    comment = models.ForeignKey(Comment)
    photo = models.ImageField(upload_to='commentphotos')
    alt = models.CharField(max_length=250, blank=True, default='')
    title = models.CharField(max_length=250, blank=True, default='')
    position = models.PositiveSmallIntegerField("Position", default=0)

    def save(self, force_insert=False, force_update=False, using=None, size=(1000, 1000)):
        if not self.id and not self.photo:
            return

        super(CommentAdditionalPhoto, self).save()

        image = Image.open(self.photo)

        image.thumbnail(size, Image.ANTIALIAS)
        image.save(self.photo.path)

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return self.photo.url

# class SEOForm(ModelForm):
#     '''
#     SEOForm
#     '''
#     title = forms.CharField(
#         label='H1'
#     )
#
#     second_title = forms.CharField(
#         label='H2'
#     )
#
#     page_title = forms.CharField(
#         label='Page title'
#     )
#
#     meta_desc = forms.CharField(
#         widget=forms.Textarea()
#
#     )
#
#     meta_keywords = forms.CharField(
#         widget=forms.TextInput(label='Meta Keywords')
#     )
#
#     noindex = models.CharField(
#         widget=forms.CheckboxInput(label='NoIndex'),
#     )
#
#
#     class Meta:
#         model = Page
#         fields = ('title', 'second_title', 'page_title', 'meta_desc', 'meta_keywords', 'noindex',)
