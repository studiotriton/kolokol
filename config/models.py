# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.db import models
from solo.models import SingletonModel
from node.models import *
from django.contrib.postgres.fields import JSONField


class SiteConfiguration(SingletonModel):
    site_name = models.CharField(
        max_length=255,
        default='Site Name'
    )

    teaser = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=u"Site Teaser",
        default='',
    )

    logo = models.FileField(
        upload_to='config/',
        blank=True,
    )

    favicon = models.ImageField(
        upload_to='config/',
        blank=True,
    )

    desc = models.TextField(
        blank=True,
        null=True,
        verbose_name=u"Site Description",
        default='',
    )

    copyright = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=u"Copyright",
        default='',
    )


    main_page = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=u"Main Page",
        default='home',
    )

    non_found_page = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=u"404 Page",
        default='404',
    )


    maintenance_mode = models.BooleanField(
        default=False
    )


    robots = models.TextField(
        default="User-agent: * \nDisallow: /"
    )


    in_head = models.TextField(
        blank=True,
        null=True,
        verbose_name=u"In Head",
        default='',
    )

    in_top_body = models.TextField(
        blank=True,
        null=True,
        verbose_name=u"In Top Body",
        default='',
    )

    in_end_body = models.TextField(
        blank=True,
        null=True,
        verbose_name=u"In End Body",
        default='',
    )

    footer_template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='footer_template'
    )

    header_template = models.ForeignKey(
        Template,
        blank=True,
        null=True,
        default=None,
        related_name='header_template'
    )

    additional_settings = JSONField(
            blank=True,
            default="",
            null=True,
    )

    recapcha_secret = models.CharField(
        max_length=40,
        blank=True,
        default="",
        null=True
    )

    def __unicode__(self):
        return u"Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"


'''
Дополнительные файлы
'''


class ConfigurationFile(models.Model):
    name = models.CharField(
        max_length=255,
        blank=True,
    )

    file = models.FileField(
        upload_to="configuration_files",
        blank=True,
    )

    link = models.CharField(
        max_length=1024,
        blank=True,
    )

    position = models.PositiveSmallIntegerField("Position")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['position']
